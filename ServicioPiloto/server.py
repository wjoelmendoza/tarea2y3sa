from flask import Flask
from flask_restful import Resource, Api
from requests import get


class Piloto(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar a esta clase
    recursos del Api REST segun el verbo que se le indique
    """

    def __init__(self):
        """"
        En este constructor se definen los conductores
        que se encuentran registrados
        """
        Resource()
        '''
        coleccion de conductores y carros
        '''
        self.pilotos = {
            '0': {'nombre': 'Carlos Estrada', 'id_carro': '0'},
            '1': {'nombre': 'Andrea Lopez', 'id_carro': '9'},
            '2': {'nombre': 'Andres Mencos', 'id_carro': '1'},
            '3': {'nombre': 'Monica Gallardo', 'id_carro': '8'},
            '4': {'nombre': 'Elver Suarez', 'id_carro': '2'},
            '5': {'nombre': 'Byron Elias', 'id_carro': '7'},
            '6': {'nombre': 'Erick Barrondo', 'id_carro': '3'},
            '7': {'nombre': 'Alejanda Burgos', 'id_carro': '6'},
            '8': {'nombre': 'Elisa Alvarado', 'id_carro': '4'},
            '9': {'nombre': 'Sergio Cortes', 'id_carro': '5'},
        }

    def get(self, id_piloto):
        """
        Se encarga de recuperar un conductor basado en el id que esta recibiendo
        :param id_piloto:
        :return: un diccionario con la informacion recolectada de la siguiente forma
        {
            'id': id_piloto,
            'nombre': nombre_piloto,
            'id_carro': id_carro,
            'desc': descripcion_carro,
            'latitud': num,
            'longitud': num
        }
        """

        print("Procesando solicitud")
        input("presiona una tecla para continuar")
        'recuperando info del conductor'
        conductor = self.pilotos.get(id_piloto, None)
        'recuperando informacion del auto y su ubicacion'
        ubicacion = self.get_ubicacion(conductor['id_carro'])

        datos = {
            'id': id_piloto,
            'nombre': conductor['nombre'],
            'id_carro': ubicacion['id'],
            'desc': ubicacion['desc'],
            'latitud': ubicacion['latitud'],
            'longitud': ubicacion['longitud']
        }

        return datos

    def get_ubicacion(self, id_carro):
        """
        consulta la ubicacion del carro al ESB
        :param id_carro:
        :return: los datos de ubicacion del carro
        """
        conexion = 'http://localhost:8080/ubicacion/' + id_carro
        respuesta = get(conexion).json()
        return respuesta


class Servidor:
    """
    Esta clase se encarga de inicializar el servidor y sus recursos
    REST
    """
    def __init__(self):
        """
        Inicializa el servidor web y sus correspondientes recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Piloto, '/piloto/<id_piloto>')

    def iniciar(self):
        """
        Inicia el servidor web y sus recursos
        """

        '''
        host = 0.0.0.0. escuchar en todas las interfaces de red
        port = 8082, puerto en el que escucha
        debug = True, indica que el servidor esta en pruebas
        '''
        self.app.run(host='0.0.0.0', port=8082, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
