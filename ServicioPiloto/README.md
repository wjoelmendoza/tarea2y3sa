# Servicio Piloto
Este servicio se encarga de manejar las solicitudes de pilotos este servidor esta diposnible en http://host:8082/.

## Recursos
### /piloto/&laquo;id&raquo;
Este recurso utiliza el verbo http **GET** y retorna la información del piloto que se identifica con &laquo;id&raquo; y también solicita al ESB por la ubicación del carro, la respuesta es el siguiente json:
`{
    id: id_conductor,
    nombre: nombre,
    id_carro: id_carro,
    desc: descripción,
    latitud: valor,
    longitud: valor
}`

donde:

* **id:** Es un valor entero que representa al código del conductor asignado.
* **nombre:** Es una cadena que representa el nombre del conductor asignado.
* **id_carro:** en un valor entero que representa el identificado del carro.
* **desc:** Es una cadena, que representa el modelo del auto asignado.
* **latidud:** Es un valor entero, que representa la latitud donde se encuentra el carro.
* **longitud:** Es un valor entero, que representa la longitud donde se encuentra el carro.
