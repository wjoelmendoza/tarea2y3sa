# Servicio Rastreo
Este servicio se encarga de manejar la información del carro y su ubicación el servicio esta diponible en http://host:8083

## Recursos
### /ubicacion/&laquo;id_carro&raquo;
Este recurso esta disponible mediante el verbo **GET** retorna la informacion del carro
que se identifica con el &laquo;id&raquo; y su ubicacion en el siguiente formato:

`{

    id: id_carro,
    desc: descripcion,
    latitud: valor,
    longitud: valor

}`

donde:

* **id:** Es un valor entero que representa el id del carro.
* **desc:** Es una cadena, que representa la descripción del carro.
* **latitud:** Es un valor entero, que representa la latitud de la ubicación del carro
* **longitud:** Es un valor entero, que representa la longitud de la ubicación del carro.

