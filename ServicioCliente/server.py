from flask import Flask
from flask_restful import Resource, Api, reqparse
from random import randint
from requests import get


class Solicitud(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar a esta clase
    recursos del Api REST segun el verbo que se le indique
    """

    def __init__(self):
        """"
        En este constructor se definen los conductores
        que se encuentran registrados
        """
        Resource()
        '''
        coleccion de conductores y carros
        '''
        self.CLIENTES = {
            '0': {'nombre': 'Carlos Estrada'},
            '1': {'nombre': 'Andrea Lopez'},
            '2': {'nombre': 'Andres Mencos'},
            '3': {'nombre': 'Monica Gallardo'},
            '4': {'nombre': 'Elver Suarez'},
            '5': {'nombre': 'Byron Elias'},
            '6': {'nombre': 'Erick Barrondo'},
            '7': {'nombre': 'Alejanda Burgos'},
            '8': {'nombre': 'Elisa Alvarado'},
            '9': {'nombre': 'Sergio Cortes'},
        }
        self.cont = 0
        self.VIAJES = {}
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=str)
        self.parser.add_argument('latitud', type=int)
        self.parser.add_argument('longitud', type=int)

    def post(self):
        """
        Se encarga de recuperar un conductor basado en el id que esta recibiendo, y si el se encuentra
        solicita un piloto y registra los datos del nuevo servicio
        :param id_piloto:
        :return: un diccionario con la informacion recolectada de la siguiente forma
        {
            'id': id_piloto,
            'nombre': nombre_piloto,
            'id_carro': id_carro,
            'desc': descripcion_carro,
            'latitud': num,
            'longitud': num
        }
        """
        print("Recibiendo solicitud")
        input("presiona una tecla para continuar")
        dt_cliente = self.parser.parse_args()  # esto deberia venir de la peticion post

        cliente = self.CLIENTES.get(dt_cliente['id'], None)

        if cliente is None:
            return {'id': dt_cliente['id'], 'estado': 'error'}

        id_piloto = randint(0, 9)

        datos = self.get_piloto(id_piloto)

        self.VIAJES[self.cont] = {
            'id': dt_cliente['id'],
            'longitud': dt_cliente['longitud'],
            'latitud': dt_cliente['latitud'],
            'datos': datos
        }

        self.cont += 1

        return datos

    def get_piloto(self, id_piloto):
        """
        require un piloto al ESB
        :param id_piloto:
        :return: los datos de ubicacion del carro y datos del piloto
        """
        conexion = 'http://localhost:8080/piloto/' + str(id_piloto)
        respuesta = get(conexion).json()
        return respuesta


class Servidor:
    """
    Esta clase se encarga de inicializar el servidor y sus recursos
    REST
    """
    def __init__(self):
        """
        Inicializa el servidor web y sus correspondientes recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Solicitud, '/solicitud')

    def iniciar(self):
        """
        Inicia el servidor web y sus recursos
        """

        '''
        host = 0.0.0.0. escuchar en todas las interfaces de red
        port = 8081, puerto en el que escucha
        debug = True, indica que el servidor esta en pruebas
        '''
        self.app.run(host='0.0.0.0', port=8081, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
