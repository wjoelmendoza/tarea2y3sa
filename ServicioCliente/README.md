# Servicio Cliente
Este servicio se encarga de recibir y guardar las solicitudes de auto de los clientes esta disponible en el puerto 8081 (http://host:8081), se encarga de validar que el cliente exista, y solicita la información del piloto al ESB mediante el recurso http://host:8080/piloto/id .

## Recursos
### /solicitud
Este recurso trabaja bajo el verbo http **POST** por lo que en el cuerpo de la solicitud recibe los siguientes datos:

` {

    id: id_cliente,
    latitud: valor,
    longitud: valor
} `

donde:

* id: es un valor numerico mayor o igual a 0 que identifica al cliente.
* latitud: es un valor entero, que representa la latitud donde se encuentra actualmente el cliente.
* longitud: es un valor entero, que representa la longitud donde se encuentra actualmente el cliente.

la información retornada por el ServicioPiloto y su definicio se encuentra [aqui](ServicioPiloto/README.md).