from flask import Flask
from flask_restful import Resource, Api, reqparse
from requests import get, post


class Ubicacion(Resource):
    """
    Esta clase herede de Resource para manejar el recurso de ubicación
    """

    def get(self, id_carro):
        """
        Realiza la peticion al servidor de ubicación
        :param id_carro:
        :return:
        """
        print("Solicitando ubicacion")
        input("presiona una tecla para continuar")
        conexion = "http://localhost:8083/ubicacion/" + str(id_carro)
        rst = get(conexion).json()
        return rst


class Piloto(Resource):
    """
    Esta clase herede de Resource para manejar el recurso de ubicación
    """

    def get(self, id_piloto):
        """

        :param id_piloto:
        :return: json de respuesta
        """
        print("Solicitando piloto")
        input("presiona una tecla para continuar")
        conexion = 'http://localhost:8082/piloto/' + str(id_piloto)
        rst = get(conexion).json()
        return rst


class Solicitud(Resource):
    """
    Maneja el recurso de solicitud del cliente
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=str)
        self.parser.add_argument('latitud', type=int)
        self.parser.add_argument('longitud', type=int)

    def post(self):
        """
        Redirige la solicitud al Servicio Cliente
        :return: la respuesta del ServicioCliente
        """
        print("Recibiendo solicitud del cliente")
        input("Presiona una tecla para continuar: ")

        conexion = "http://localhost:8081/solicitud"
        dt_cliente = self.parser.parse_args()

        rst = post(conexion, data=dt_cliente).json()

        return rst


class Servidor:
    """
    Esta clase habilita el servidor para simular un ESB
    """
    def __init__(self):
        """
        crear el servidor web y los recursos que maneja
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Ubicacion, '/ubicacion/<id_carro>')
        self.api.add_resource(Piloto, '/piloto/<id_piloto>')
        self.api.add_resource(Solicitud, '/solicitud')

    def iniciar(self):
        """
        Se encargad de iniciar y configurar el servidor web
        """
        '''
        host= 0.0.0.0
        port = 8080
        debug = True
        '''
        self.app.run(host='0.0.0.0', port=8080, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
